<?php
/**
 * @package WordPress
 * @subpackage Mooncup Main
 * @since Mooncup Main 1.0
 * Template Name: Contact Page
 */

get_header(); ?>
<section class="contact page-content primary" role="main">
		
	        	<article class="container_boxed container__2col container__2col--spacer content_band">
	        		<div class="container__inner">
	        		<?php the_field('title');?>
		        		<div class="block-out grey-bg">
		        			<?php the_field('contact_detail');?>
		        		</div>

	        		</div>

	        		<div class="container__inner">
	        		<?php the_field('content_area_right');?>
	        		</div>
	        	</article>

	        	<div class="container_boxed container__2col container__2col--spacer content_band">
	        		<div class="container__inner">
		        		<?php the_field('block_title_left');?>
		        		<div class="block-out grey-bg">
		        			<?php the_field('block_content_left');?>
		        		</div>
	        		</div>

	        		<div class="container__inner">
		        		<?php the_field('block_title_right');?>
		        		<div class="block-out grey-bg">
		        			<?php the_field('block_content_right');?>
		        		</div>
	        		</div>

	        	</div>

	        <aside class="page-outro container_boxed content_band--lined">
	        	<div class="container_boxed--narrow content_band--small">
	        		<?php the_field('footer_area');?>
	        	</div>
	        </aside>
	
</section>

<?php get_footer(); ?>
