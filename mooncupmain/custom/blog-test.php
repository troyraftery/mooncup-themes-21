<?php
/**
 * @package WordPress
 * @subpackage Mooncup Main
 * @since Mooncup Main 1.0
 * Template Name: blog test
 */

get_header(); ?>
<section class="single-col page-content primary" role="main">

		    <div class="container_boxed content_band--small">
		    	<div class="featured-post">
		    		<div class="blog-post">
		    			<?php

						$post_object = get_field('main_feature_post');

						if( $post_object ): 

							// override $post
							$post = $post_object;
							setup_postdata( $post ); 

							?>

						<a href="<?php the_permalink(); ?>" rel="bookmark">
							<div class="blog-image">
								<?php if (has_post_thumbnail( $post->ID ) ): ?>
									<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
						        	<div class = "content post-featured-image image-cover" style="background-image:url('<?php echo $image[0]; ?>');"></div>
							    <?php endif; ?>
							</div>
						</a>

							<div class="post-content">
								<a href="<?php the_permalink(); ?>" rel="bookmark">
									<h1 class="post-title"><?php the_title(); ?></h1>
								</a>
						
						
								<div class="post-meta"><?php
									mooncupmain_post_meta(); ?>
									<div class="share-links"><?php echo do_shortcode ('[shareaholic app="share_buttons" id="21970451"]'); ?></div>
								</div>

									<?php the_excerpt(); ?>
									<a href="<?php the_permalink(); ?>" class="read-more"><?php _e( 'Read more', 'mooncupmain' ); ?></a>
									
									<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
									<?php endif; ?>
							</div>
					</div>
				</div>
			

				<section class="featured-post-listing blog-section">
					<div class="category-title">
						<h1>More Featured</h1>
					</div>
					<div class="container">
					<?php

						$post_objects = get_field('featured_items_all');

						if( $post_objects ): ?>
						    
						    <?php foreach( $post_objects as $post): // variable must be called $post (IMPORTANT) ?>
						        <?php setup_postdata($post); ?>
						        <article class="blog-post">
					    		

									<a href="<?php the_permalink(); ?>" rel="bookmark">
										<div class="blog-image">
											<?php if (has_post_thumbnail( $post->ID ) ): ?>
												<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
									        	<div class = "content post-featured-image image-cover" style="background-image:url('<?php echo $image[0]; ?>');"></div>
										    <?php endif; ?>
										</div>
									</a>

										<div class="post-content">
											<a href="<?php the_permalink(); ?>" rel="bookmark">
												<h1 class="post-title"><?php the_title(); ?></h1>
											</a>

											<?php the_excerpt(); ?>
											<a href="<?php the_permalink(); ?>" class="read-more"><?php _e( 'Read more', 'mooncupmain' ); ?></a>
												
												
										</div>
								</article>
						    <?php endforeach; ?>
						    
						    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
						<?php endif;?>	
					</div>
		 		</section>   
		 	
		 	<div class="container_boxed blog-section">
		 		<section class="category-block col__6">
					<div class="category-title">
						<h1>More Featured</h1>
					</div>
					<div class="container">
					<?php

						$post_objects = get_field('category_item_1');

						if( $post_objects ): ?>
						    
						    <?php foreach( $post_objects as $post): // variable must be called $post (IMPORTANT) ?>
						        <?php setup_postdata($post); ?>
						        <article class="blog-post">
					    		

									<a href="<?php the_permalink(); ?>" rel="bookmark">
										<div class="blog-image">
											<?php if (has_post_thumbnail( $post->ID ) ): ?>
												<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
									        	<div class = "content post-featured-image image-cover" style="background-image:url('<?php echo $image[0]; ?>');"></div>
										    <?php endif; ?>
										</div>
									</a>

										<div class="post-content">
											<a href="<?php the_permalink(); ?>" rel="bookmark">
												<h1 class="post-title"><?php the_title(); ?></h1>
											</a>

											<?php the_excerpt(); ?>
											<a href="<?php the_permalink(); ?>" class="read-more"><?php _e( 'Read more', 'mooncupmain' ); ?></a>
												
												
										</div>
								</article>
						    <?php endforeach; ?>
						    
						    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
						<?php endif;?>	
					</div>
		 		</section>  

		 		<section class="category-block col__6">
					<div class="category-title">
						<h1>More Featured</h1>
					</div>
					<div class="container">
					<?php

						$post_objects = get_field('category_item_2');

						if( $post_objects ): ?>
						    
						    <?php foreach( $post_objects as $post): // variable must be called $post (IMPORTANT) ?>
						        <?php setup_postdata($post); ?>
						        <article class="blog-post">
					    		

									<a href="<?php the_permalink(); ?>" rel="bookmark">
										<div class="blog-image">
											<?php if (has_post_thumbnail( $post->ID ) ): ?>
												<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
									        	<div class = "content post-featured-image image-cover" style="background-image:url('<?php echo $image[0]; ?>');"></div>
										    <?php endif; ?>
										</div>
									</a>

										<div class="post-content">
											<a href="<?php the_permalink(); ?>" rel="bookmark">
												<h1 class="post-title"><?php the_title(); ?></h1>
											</a>

											<?php the_excerpt(); ?>
											<a href="<?php the_permalink(); ?>" class="read-more"><?php _e( 'Read more', 'mooncupmain' ); ?></a>
												
												
										</div>
								</article>
						    <?php endforeach; ?>
						    
						    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
						<?php endif;?>	
					</div>
		 		</section>  
		 	</div> 
		</div><!--end of container--> 		
		  
	        <article class="container_full content_band">
	        	<div class="container_boxed--narrow">

	        		

	        	</div>
	        </article>
	        <article class="container_full content_band">
	        	<div class="container_boxed--narrow">
	        		<?php

					$post_objects = get_field('category_item_1');

					if( $post_objects ): ?>
					    <ul>
					    <?php foreach( $post_objects as $post): // variable must be called $post (IMPORTANT) ?>
					        <?php setup_postdata($post); ?>
					        <li>
					        	<span><?php the_post_thumbnail(); ?></span>
					            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
					            <span>Post Object Custom Field: <?php the_excerpt(); ?></span>
					        </li>
					    <?php endforeach; ?>
					    </ul>
					    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
					<?php endif;?>

	        	</div>
	        </article>
	
</section>

<?php get_footer(); ?>
